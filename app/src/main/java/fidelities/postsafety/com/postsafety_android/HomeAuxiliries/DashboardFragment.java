package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries.AdimnReceiveHomeFragment;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 25/06/2018.
 */

public class DashboardFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_report, ll_receive;
    ToolbarUpdateListener toolbarUpdateListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard_home, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        ll_report = (LinearLayout) v.findViewById(R.id.ll_reports);
        ll_receive= (LinearLayout)v.findViewById(R.id.ll_receive);
        toolbarUpdateListener =(ToolbarUpdateListener) getActivity();

        ll_report.setOnClickListener(this);
        ll_receive.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_reports:
                navToReportTypeFragment();
                break;
            case R.id.ll_receive:
                navToReceiveHomeFragment();
                break;
        }
    }

    private void navToReportTypeFragment() {
        Fragment frg = new ReportTypeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.ReportTypeFragment);
        ft.addToBackStack(AppConstt.FRGTAG.ReportTypeFragment);
        ft.hide(this);
        ft.commit();
    }

    private void navToReceiveHomeFragment() {
        Fragment frg = new AdimnReceiveHomeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.AdminReceivHomeFragment);
        ft.addToBackStack(AppConstt.FRGTAG.AdminReceivHomeFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(isHidden()){

        }else {
            toolbarUpdateListener.backButtonVisibilty(View.GONE);
        }
    }
}
