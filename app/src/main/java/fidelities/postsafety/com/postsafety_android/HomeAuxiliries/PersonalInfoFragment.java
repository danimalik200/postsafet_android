package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import fidelities.postsafety.com.postsafety_android.R;

/**
 * Created by Lenovo on 27/06/2018.
 */

public class PersonalInfoFragment extends Fragment implements View.OnClickListener {

    EditText edtPhoneNum, edtOldPassw, edtNewPassw;
    Button btnSaveChanges;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_personal_info, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        edtPhoneNum = (EditText) v.findViewById(R.id.edt_phone_num);
        edtOldPassw = (EditText) v.findViewById(R.id.edt_old_passw);
        edtNewPassw = (EditText) v.findViewById(R.id.edt_new_passw);
        btnSaveChanges = (Button) v.findViewById(R.id.btn_save_changes);

        btnSaveChanges.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save_changes:
            break;
        }
    }
}
