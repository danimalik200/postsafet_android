package fidelities.postsafety.com.postsafety_android.IntroAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fidelities.postsafety.com.postsafety_android.R;

/**
 * Created by Lenovo on 25/06/2018.
 */

public class ForgetPasswordFragment extends Fragment implements View.OnClickListener {
    Button btnSavechanges;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_forget_passw, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){

        btnSavechanges = (Button) v.findViewById(R.id.btn_savechanges);
        btnSavechanges.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_savechanges:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
