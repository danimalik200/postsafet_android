package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 26/06/2018.
 */

public class CategoriesFragment extends Fragment implements View.OnClickListener{
    LinearLayout ll_category1, ll_cat2, ll_cat3, ll_cat4;
    ToolbarUpdateListener toolbarUpdateListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_categories, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){

        ll_category1  = (LinearLayout) v.findViewById(R.id.ll_cat1);
        ll_cat2  = (LinearLayout) v.findViewById(R.id.ll_cat2);
        ll_cat3  = (LinearLayout) v.findViewById(R.id.ll_cat3);
        ll_cat4  = (LinearLayout) v.findViewById(R.id.ll_cat4);
        toolbarUpdateListener =(ToolbarUpdateListener) getActivity();
        toolbarUpdateListener.hideToolbar(View.VISIBLE);
        toolbarUpdateListener.backButtonVisibilty(View.VISIBLE);

        ll_category1.setOnClickListener(this);
        ll_cat2.setOnClickListener(this);
        ll_cat3.setOnClickListener(this);
        ll_cat4.setOnClickListener(this);


    }

    private void navToSubCategoriesFragment() {
        Fragment frg = new SubCategoriesFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.SubCategoriesFragment);
        ft.addToBackStack(AppConstt.FRGTAG.SubCategoriesFragment);
        ft.hide(this);
        ft.commit();
    }

    private void navToPSIPromoptFragment() {
        Fragment frg = new PsiPromoptFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.PsiPromptFragment);
        ft.addToBackStack(AppConstt.FRGTAG.PsiPromptFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_cat1:
                if(AppConfig.getInstance().isComingFromNearMiss){
                    navToPSIPromoptFragment();
                }else {
                    navToSubCategoriesFragment();
                }

                break;
            case R.id.ll_cat2:
                if(AppConfig.getInstance().isComingFromNearMiss){
                    navToPSIPromoptFragment();
                }else {
                    navToSubCategoriesFragment();
                }
                break;
            case R.id.ll_cat3:
                if(AppConfig.getInstance().isComingFromNearMiss){
                    navToPSIPromoptFragment();
                }else {
                    navToSubCategoriesFragment();
                }
                break;
            case R.id.ll_cat4:
                if(AppConfig.getInstance().isComingFromNearMiss){
                    navToPSIPromoptFragment();
                }else {
                    navToSubCategoriesFragment();
                }
                break;
        }
    }
}
