package fidelities.postsafety.com.postsafety_android.Utils;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by indus on 8/11/2017.
 */

public interface AppConstt {

    //////////////production///////////////////////////
//    String BASE_URL = "https://delta-homelab.com/api/";
//    String IMAGE_BASE_URL = "https://delta-homelab.com/images/";
//    String REPORTS_BASE_URL = "https://delta-homelab.com/reports/";
    //////////////production///////////////////////////


    ///////////////staging/////////////////////////
    String BASE_URL = "http://35.154.1.27:8080/api/collector/";
    String IMAGE_BASE_URL = "http://35.154.1.27:8080/images/";

    String REPORTS_BASE_URL = "http://35.154.1.27:8080/reports/";
    ///////////////staging/////////////////////////

    int CAMERA = 1;

    String IMAGE_DIR_NAME = "/PostSafety";
    String IMAGE_DIR_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + IMAGE_DIR_NAME;

    final int GALLERY_INTENT_CALLED = 50;
    final int GALLERY_KITKAT_INTENT_CALLED = 51;
    final int CAMERA_INTENT_CALLED = 52;
    int MAX_PIC_HEIGHT = 800;//Portrait case
    int MAX_PIC_WIDTH = 800;//Portrait case


    String PENDING = "pending";
    String CONFIRMED = "confirmed";
    String COLLECTED = "collected";
    String ANALYZING = "analyzing";
    String COMPLETED = "completed";




    int MYNUMBER = 1;
    int OTHERNUMBERCONTACT = 2;
    int OTHERNUMBERMANUAL = 3;
    String ARABIC = "ar";
    String ENGLISH = "en";
    String ORDERUPDATE = "orderUpdate";
    String ORDERCOMPLETED = "orderCompleted";
    int MALE = 1;
    int FEMALE = 0;
    int ANY = 2;
    int PROMOTION_BLUE = 0;
    int PROMOTION_GREEN = 1;
    int PROMOTION_RED = 2;
    int PER_PAGE_ITEM = 10;
    String MONTH = "Month";
    String YEAR = "Year";
    String FACEBOOK_URL = "https://www.google.com.pk/";
    String INSTA_URL = "https://www.google.com.pk/";
    String TWITTER_URL = "https://www.google.com.pk/";
    String MALE_Val = "Male";
    String FEMALE_Val = "Female";
    String ANY_Val = "Any";
    String ORDER_UPDATE = "1";
    String ALERT_NOTIFICATION = "2";
    String ORDER_COMPLETED = "3";
    int STATE_SHOW_BOTH_BUTTON = 1;
    int STATE_SHOW_LOGIN_BACK = 2;
    int STATE_HIDE_BOTH = 3;
    String NOTIFICATION_ON = "on";
    String NOTIFICATION_OFF = "off";
    int LIMIT_API_RETRY = 0;
    int LIMIT_TIMOUT_MILLIS = 1500;
    int Success = 1;
    int Error = 0;
    byte TOOLBAR_STATE_HIDE = 0;
    byte TOOLVAR_STATE_HOME_SHOW = 1;
    byte TOOLBAR_STATE_SEARCH_SHOW = 2;
    byte BOTTOM_TAB_HOME_HIDE = 4;
    byte BOTTOM_TAB_DELTA_LABS = 0;
    byte BOTTOM_TAB_REPORTS = 1;
    byte BOTTOM_TAB_ALERTS = 2;
    byte BOTTOM_TAB_SETTINGS = 3;
    String CASH_ON_VISIT = "COV";
    String BANK_TRANSFER = "BT";
    int REQ_CODE_PERM_LOCATION = 1;
    int REQUEST_EXTERNAL_STORAGE = 1;
    int REQUEST_CONTACTS = 1;
    int READ_EXTERNAL_STORAGE = 2;
    int WRITE_EXTERNAL_STORAGE = 3;

    interface STATE_APP {
        int DELTA_HOME_LAB = 0;
        int REPORTS = 1;
        int ALERTS = 2;
        int SETTINGS = 3;
        int SETTINGS_WEBVIEW = 4;
        int MYACCOUNT_FRAGMENT = 5;
        int PROMOTIONS = 6;

        int ORDERS_FRAGMENT =7;
    }

    interface NOTIFICATION_STATUS {
        int ORDERS = 1;
        int ALERTS = 2;
        int ORDER_COMPLETED = 3;
        int NONE = 0;
    }

    interface SETTINGS_URL {
        String ABOUT = "https://delta-homelab.com/webview/about.html";
        String HELP_URL = "https://delta-homelab.com/webview/help.html";
        String TERMS_URL = "https://delta-homelab.com/webview/terms.html";
    }

    interface FRGTAG {
        String ForgotPasswordCodeFragment = "ForgotPasswordCodeFragment";
        String ResetPasswordFragment = "ResetPasswordFragment";
        String DashboardFragment = "DashboardFragment";
        String ReportTypeFragment ="ReportTypeFragment";
        String PhotoVideoFragment ="PhotoVideoFragment";
        String CategoriesFragment ="CategoriesFragment";
        String SubCategoriesFragment ="CategoriesFragment";
        String LocationsFragment ="LocationsFragment";
        String ReportSummaryFragment ="ReportSummaryFragment";
        String ConfirmationFragment ="ConfirmationFragment";
        String PsiPromptFragment ="PsiPromptFragment";
        String AdminReceivHomeFragment ="AdminReceivHomeFragment";
        String AlertsHomeFragment ="AlertsHomeFragment";
        String FeedsFragment ="FeedsFragment";
        String SingleAlertDetailFragment ="SingleAlertDetailFragment";
        String TrainingHomeFragment ="TrainingHomeFragment";
        String SafetyStatisticsFragment ="SafetyStatisticsFragment";
        String SafetyReportTypeFragment ="SafetyReportTypeFragment";
        String ViewPostsFragment ="ViewPostsFragment";



        String SignUpFragment = "SignUpFragment";
        String DeltaHomeLabFragment = "DeltaHomeLabFragment";
        String FollowUsWebViewFragment = "FollowUsWebViewFragment";
        String SignInFragment = "SignInFragment";
        String NewLabOrderFragment = "NewLabOrderFragment";
        String NumberVerificationFragment = "NumberVerificationFragment";
        String ForgetPasswordFragment = "ForgetPasswordFragment";
        String ChangePasswordFragment = "ChangePasswordFragment";
        String Settingsfragment = "Settingsfragment";
        String MyAccountFragment = "MyAccountFragment";
        String AccoutChangePasswordFragment = "AccoutChangePasswordFragment";
        String SelectLabFragment = "SelectLabFragment";
        String LabTestFragment = "LabTestFragment";
        String TestDetailsFragment = "TestDetailsFragment";
        String OrderDetailsFragment = "OrderDetailsFragment";
        String GoogleMapsFragment = "GoogleMapsFragment";
        String DateTimeFragment = "DateTimeFragment";
        String OrderSummaryFragment = "OrderSummaryFragment";
        String PaymentMethodFragment = "PaymentMethodFragment";
        String OrderConfirmedFragment = "OrderConfirmedFragment";
        String SplashFragment = "SplashFragment";
        String ViewResultsFragment = "ViewResultsFragment";
        String PendingReportsFragment = "PendingReportsFragment";
        String CompletedReportsFragment = "CompletedReportsFragment";
        String WebViewFragment = "WebViewFragment";
        String RescheduleFragment = "RescheduleFragment";
        String AlertsFragment = "AlertsFragment";
        String SettingsWebViewFragment = "SettingsWebViewFragment";
        String SpecialOfferDetailFragment = "SpecialOfferDetailFragment";
        String SpecialOffersFragment = "SpecialOffersFragment";
        String RequestHomeLabFragment = "RequestHomeLabFragment";
        String TestSpecificationFragment = "TestSpecificationFragment";
        String SelectTestFragment = "SelectTestFragment";
        String ScheduleCallFragment = "ScheduleCallFragment";
        String PhotoFragment = "PhotoFragment";


    }

    interface CONTACT_TYPE {
        String CONTACT_MY_NUMBER = "0";
        String DISCUSS_WITH_OTHER_PERSON = "1";
    }

    interface SELECTION_TYPE {
        String NO_NEED_NOW = "0";
        String KINDLY_CALL = "1";
        String PHOTO_UPLOADED = "2";
        String PROMOTION_SELECTED = "3";
        String TEST_SELECTED_FROM_LIST = "4";
    }

    interface TOOLBARSTATE {
        byte SHOW_CART = 1;
        byte SHOW_SEARCH = 2;
        byte SHOW_ONLY_HEADER = 3;
        byte HIDE_TOOLBAR = 4;
    }


    interface TOASTMSG {


    }

    interface ServerStatus {
        //Server Response Status
        String REQUEST_SUCCESS = "success";
        String REQUEST_ERROR = "error";

        short OK = 200;
        short CREATED = 201;//
        short ACCEPTED = 202;//
        short NO_CONTENT = 204;//
        short RESET_CONTENT = 205;//
        short BAD_REQUEST = 400;//
        short UNAUTHORIZED = 401;
        short FORBIDDEN = 403;
        short DATABASE_NOT_FOUND = 404;
        short METHOD_NOT_ALLLOWED = 405;
        short NOT_ACCEPTABLE = 406;
        short CONFLICT = 409;//token expired
        short INTERNAL_SERVER_ERROR = 500;
        short BAD_GATEWAY = 502;
        short HTTP_VERSION_NOTSUPPORTED = 505;
        short NETWORK_ERROR = 0;

    }


    interface MSG_ERROR {
        //General App messages
        String PREFIX = "Error: ";
        String NETWORK = "Please check your Internet connection";

        String ENABLE_LOCATION = "Please enable location from your device";
        String EMAIL = "Please enter valid email";
        String MOBNUM = "Please enter valid mobile number";
        String PASSWORD_EMPTY = "Password field cannot be left empty";
        String PASSWORD_SHORT = "Please enter atleast 6 characters password";
        String PASSWORD_NOMATCH = "Both the passwords donot match";
        String CODE_EMPTY = "Please fill-in the code above, you recieved via SMS";
        String CODE_WRONG = "Please enter the correct code, you recieved via SMS";
        String CONTACTS_SYNC = "Contacts syncing faile.\nPlease retry";
    }

    interface MSG_ERROR_AR {
        String NETWORK = "الرجاء التحقق من اتصال الانترنت الخاص بك";
    }
}
