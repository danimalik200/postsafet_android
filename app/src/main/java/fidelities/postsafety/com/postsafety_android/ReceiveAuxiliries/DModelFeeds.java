package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class DModelFeeds {

    String id, headerText, time , date;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
