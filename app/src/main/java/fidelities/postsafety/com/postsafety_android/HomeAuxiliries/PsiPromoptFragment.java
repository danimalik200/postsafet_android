package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 27/06/2018.
 */

public class PsiPromoptFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_yes, ll_no;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_psi_prompt, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        ll_yes = (LinearLayout) v.findViewById(R.id.ll_yes);
        ll_no = (LinearLayout) v.findViewById(R.id.ll_no);

        ll_yes.setOnClickListener(this);
        ll_no.setOnClickListener(this);

        AppConfig.getInstance().isComingFromNearMiss =false;


    }

    private void navtoLocationsFragment() {
        Fragment frg = new LocationsFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.LocationsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.LocationsFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_yes:
                navtoLocationsFragment();
                break;
            case R.id.ll_no:
                navtoLocationsFragment();
                break;
        }
    }
}
