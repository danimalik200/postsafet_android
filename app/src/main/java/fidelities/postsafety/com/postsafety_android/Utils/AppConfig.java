package fidelities.postsafety.com.postsafety_android.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.TypedValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by indus on 8/11/2017.
 */

public class AppConfig {
    private static AppConfig ourInstance;// = new AppConfig(null);

    private AppConfig(Context _mContext) {
        if (_mContext != null) {

            this.mContext = _mContext;

            this.sharedPref = mContext.getSharedPreferences("pref_tabeeb", Context.MODE_PRIVATE);
            this.editor = sharedPref.edit();


            this.marginToast = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10
                    , mContext.getResources().getDisplayMetrics()));

            isComingFromEmergency =false;
            isComingFromNearMiss =false;
            isComingFromSafetyStatistics =false;


        }
    }

    public static void initInstance(Context _mContext) {
        if (ourInstance == null) {
            // Create the instance
            ourInstance = new AppConfig(_mContext);
        }
    }

    public void refreshData() {

        this.marginToast = (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10
                , mContext.getResources().getDisplayMetrics()));
        isEnglish = true;
        mobileNo = "";
        tempMobNo = "";
        labNameEng = "";
        labNameAr = "";
        mSettingsUrl = "";
        mNotificationStatus = -1;
        reportName = "";
        labId = "";
        mUrl = "";


        rescheduleOrderId = "";
        mSettingsHeader = "";
        langChangeFrmSettings = false;
        isComingFromForgetPassword = false;
        isComingFromOrderSummary = false;
        isComingFromForgetPassword = false;
        isComingFromLabTest = false;
        isLangChangeFromSignin = false;
        isLanguageClickedFrmHome = false;
        isComingFromEditOrder = false;
        isComingFromReschedule = false;
        isLanguageClicked = false;
        isOrderConfirmedFragment = false;
        isPlaceNewOrderClicked = false;
        isComingFromSelectTestFragment = false;
        isComingFromPromotions = false;
        isEditOrderFromViewResults = false;
        isLoggedInToOtherDevice = true;
        mStateapp = AppConstt.STATE_APP.DELTA_HOME_LAB;
        isComingFromLogout = false;

        editor.putString("delta_lab_user_name", "");
        editor.putLong("delta_lab_user_mobile", 0);
        editor.putString("delta_lab_user_city", "");
        editor.putBoolean("delta_lab_user_isLoggedin", false);
        editor.putString("delta_lab_user_token", "");
        saveNewAlert(false);
        editor.commit();
        //loadUserProfile();
        if (isLoggedInToOtherDevice) {

        } else {
            //saveOrderData();
        }
    }

    private Context mContext;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    public Typeface tfAppDefault;

    public boolean isComingFromNearMiss =false;
    public boolean isComingFromEmergency= false;
    public boolean isComingFromSafetyStatistics;
    public String safetyStatisticType;
    public int marginToast;
    public int mStateapp;
    public int mNotificationStatus;
    public boolean isArabic;
    public boolean isEnglish;
    public boolean isComingFromForgetPassword;
    public boolean isComingFromMyAccount;
    public boolean langChangeFrmSettings;
    public boolean isComingFromLabTest;
    public boolean isLangChangeFromSignin;
    public boolean isComingFromOrderSummary;
    public boolean isComingFromEditOrder; // most important. using it in most of fragments if user wants to edit the order
    public boolean isComingFromReschedule;
    public boolean isComingFromPromotions;
    public boolean isLanguageClicked;
    public boolean isPlaceNewOrderClicked;
    public boolean isLanguageClickedFrmHome;
    public boolean isOrderConfirmedFragment;
    public boolean isLoggedInToOtherDevice;
    public boolean isComingFromSelectTestFragment;
    public boolean isEditOrderFromViewResults;
    public String labNameEng;
    public String mobileNo;
    public String tempMobNo;
    public String labNameAr;
    public String reportName;
    public String labId;
    public String rescheduleOrderId;
    public String mSettingsUrl;
    public String mSettingsHeader;
    public String mUrl;
    public boolean isComingFromLogout;
    //public String[] listImages;
    public List<String> listImages ;
    public String[] listImages2;
    public boolean isSaudiNational;

    public static AppConfig getInstance() {
        return ourInstance;
    }


    public String numberEngToArabic(String strNumber) {
        try {


            char[] arabicChars = {'٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'};
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < strNumber.length(); i++) {
                if (Character.isDigit(strNumber.charAt(i))) {
                    builder.append(arabicChars[(int) (strNumber.charAt(i)) - 48]);
                } else {
                    builder.append(strNumber.charAt(i));
                }
            }
            return builder.toString();
        } catch (Exception e) {

            e.printStackTrace();
            return strNumber;
        }

    }

    public boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    public String arabicToDecimal(String number) {
//        number = arabicToDecimal(number); // number = 42;

        final String arabic = "\u06f0\u06f1\u06f2\u06f3\u06f4\u06f5\u06f6\u06f7\u06f8\u06f9";
        char[] chars = new char[number.length()];
        for (int i = 0; i < number.length(); i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);
    }

    public void saveDefLanguage(String lang) {
        editor.putString("delta_lab_def_lang", lang);
        editor.commit();
    }

    public String loadDefLanguage() {
        return sharedPref.getString("delta_lab_def_lang", "");
    }

/*    public void saveUserProfile() {
        editor.putString("delta_lab_user_name", AppConfig.getInstance().mUser.name);
        editor.putString("delta_lab_user_gender", AppConfig.getInstance().mUser.gender);
        editor.putBoolean("delta_lab_user_isLoggedin", true);
        editor.putString("delta_lab_user_token", AppConfig.getInstance().mUser.token);
        editor.putString("delta_lab_user_image", AppConfig.getInstance().mUser.collectorImage);
        editor.putString("userID", AppConfig.getInstance().mUser.id);
        editor.commit();
        loadUserProfile();
    }

    public void loadUserProfile()
    {
        mUser.name = sharedPref.getString("delta_lab_user_name", "");
        mUser.gender = sharedPref.getString("delta_lab_user_gender", "");
        mUser.isLoggedIn = sharedPref.getBoolean("delta_lab_user_isLoggedin", false);
        mUser.token = sharedPref.getString("delta_lab_user_token", "");
        mUser.collectorImage = sharedPref.getString("delta_lab_user_image", "");
        mUser.id = sharedPref.getString("userID", "");
    }*/

    public String loadIsNotificationOn() {
        return sharedPref.getString("delta_lab_notification", "on");
    }

    public void saveNotificationStatus(String val) {
        editor.putString("delta_lab_notification", val);
        editor.commit();
    }

    public void saveFCMDeviceToken(String token) {
        editor.putString("delta_lab_fcm_token", token);
        editor.commit();
    }

    public String loadFCMDeviceToken() {

        return sharedPref.getString("delta_lab_fcm_token", "");
    }

    /*public void saveOrderData() {

        Gson gson = new Gson();
        String json = gson.toJson(dModelCreateOrder);
        editor.putString("delta_lab_create_order", json);
        Log.i("checkjson", json);
        editor.commit();

    }*/



    public void isEditOrder(boolean b) {
        editor.putBoolean("delta_lab_edit", b);
        editor.commit();
    }

    public boolean loadEditOrder() {
        return sharedPref.getBoolean("delta_lab_edit", false);
    }

    public void saveOrderId(String id) {
        editor.putString("delta_lab_order_id", id);
        editor.commit();
    }

    public String loadOrderId() {
        return sharedPref.getString("delta_lab_order_id", "");
    }

    public void saveNewAlert(boolean b) {
        editor.putBoolean("delta_lab_save_alert", b);
        editor.commit();
    }

    public boolean isNewAlert() {
        return sharedPref.getBoolean("delta_lab_save_alert", false);
    }

    public void saveLocation(String location) {
        editor.putString("delta_lab_order_location", location);
        editor.commit();
    }

    public String loadLocation() {
        return sharedPref.getString("delta_lab_order_location", "");
    }

    public void saveLocationLat(double latitude) {
        editor.putString("delta_lab_order_lat", String.valueOf(latitude));
        editor.commit();
    }

    public double loadLocationLat() {
        double lat = 0.0;
        try {
            lat = Double.parseDouble(sharedPref.getString("delta_lab_order_lat", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lat;
    }

    public void saveLocationLong(double longitude) {
        editor.putString("delta_lab_order_long", String.valueOf(longitude));
        editor.commit();
    }

    public double loadLocationLong() {
        double Long = 0.0;
        try {
            Long = Double.parseDouble(sharedPref.getString("delta_lab_order_long", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Long;

    }

    public void saveBankTransferDetailsEng(String eng) {
        editor.putString("delta_lab_order_bank_dtl_eng", eng);
        editor.commit();
    }

    public void saveBankTransferDetailsAr(String ar) {
        editor.putString("delta_lab_order_bank_dtl_ar", ar);
        editor.commit();
    }

    public String loadBankTransferDetailsEng() {
        return sharedPref.getString("delta_lab_order_bank_dtl_eng", "");
    }

    public String loadBankTransferDetailsAr() {
        return sharedPref.getString("delta_lab_order_bank_dtl_ar", "");
    }
}
