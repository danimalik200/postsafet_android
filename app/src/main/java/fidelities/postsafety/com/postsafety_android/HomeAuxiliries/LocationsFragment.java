package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 26/06/2018.
 */

public class LocationsFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_location1, ll_location2, ll_location3, ll_location4;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_locations, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){

        ll_location1  = (LinearLayout) v.findViewById(R.id.ll_location1);
        ll_location2  = (LinearLayout) v.findViewById(R.id.ll_location2);
        ll_location3  = (LinearLayout) v.findViewById(R.id.ll_location3);
        ll_location4  = (LinearLayout) v.findViewById(R.id.ll_location4);

        ll_location1.setOnClickListener(this);
        ll_location2.setOnClickListener(this);
        ll_location3.setOnClickListener(this);
        ll_location4.setOnClickListener(this);


    }

    private void navToReportSummaryFragment() {
        Fragment frg = new ReportSummaryFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.ReportSummaryFragment);
        ft.addToBackStack(AppConstt.FRGTAG.ReportSummaryFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_location1:
                navToReportSummaryFragment();
                break;
            case R.id.ll_location2:
                navToReportSummaryFragment();
                break;
            case R.id.ll_location3:
                navToReportSummaryFragment();
                break;
            case R.id.ll_location4:
                navToReportSummaryFragment();
                break;
        }
    }
}
