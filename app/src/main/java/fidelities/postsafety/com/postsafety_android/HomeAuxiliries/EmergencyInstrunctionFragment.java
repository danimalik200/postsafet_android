package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 27/06/2018.
 */

public class EmergencyInstrunctionFragment extends Fragment implements View.OnClickListener {
    Button btnTakeVideo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_emergency_instructions, container, false);
        return v;
    }

    void bindViews(View v){
        btnTakeVideo = (Button) v.findViewById(R.id.btn_take_video);
        btnTakeVideo.setOnClickListener(this);

    }

    private void navToPhotoVideoFragment() {
        Fragment frg = new FragmentPhotoVideo();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.PhotoVideoFragment);
        ft.addToBackStack(AppConstt.FRGTAG.PhotoVideoFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_take_video:
                navToPhotoVideoFragment();
                break;
        }
    }
}
