package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import fidelities.postsafety.com.postsafety_android.R;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class FeedsAdapter extends BaseAdapter {
    ArrayList<DModelFeeds> listFeeds;
    Context context;
    LayoutInflater mInflater;

    public FeedsAdapter(ArrayList<DModelFeeds> listFeeds, Context context){
        this.context = context;
        this.listFeeds =listFeeds;
        this.mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return listFeeds.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolderw;
         DModelFeeds dModelFeeds = listFeeds.get(position);
        if (convertView == null) {
            viewHolderw = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_feeds, null);
            viewHolderw.txvTime = (TextView) convertView.findViewById(R.id.txv_time);
            viewHolderw.txvHeader = (TextView) convertView.findViewById(R.id.txv_header_text);
            viewHolderw.txvDate = (TextView) convertView.findViewById(R.id.txv_date);
            convertView.setTag(viewHolderw);
        } else {
            viewHolderw = (ViewHolder) convertView.getTag();
        }

        viewHolderw.txvTime.setText(dModelFeeds.getTime());
        viewHolderw.txvHeader.setText(dModelFeeds.getHeaderText());
        viewHolderw.txvDate.setText(dModelFeeds.getDate());


        return convertView;
    }

    public static class ViewHolder{
        TextView txvTime, txvHeader, txvDate;
    }
}
