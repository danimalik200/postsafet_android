package fidelities.postsafety.com.postsafety_android.IntroAuxiliries;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import fidelities.postsafety.com.postsafety_android.HomeAuxiliries.HomeActivity;
import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 25/06/2018.
 */

public class SigninFragment extends Fragment implements View.OnClickListener{
    EditText edtName, edtPhoneNum, edtPassword;
    Button btnLogin, btnAgreeAndContinues;
    TextView txvForgetPassw, txvTermsOfPoliciy;
    Dialog dialogTerms;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_login, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        btnLogin = (Button) v.findViewById(R.id.btn_login);
        txvForgetPassw = (TextView) v.findViewById(R.id.txv_reset_passw);
        txvTermsOfPoliciy = (TextView) v.findViewById(R.id.txv_term_of_policy);

        btnLogin.setOnClickListener(this);
        txvForgetPassw.setOnClickListener(this);
        txvTermsOfPoliciy.setOnClickListener(this);
        initializeTermsDialog();

    }

    void initializeTermsDialog(){
        dialogTerms = new Dialog(getActivity());
        dialogTerms.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTerms.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogTerms.setContentView(R.layout.fragment_terms_policy);
        dialogTerms.setCancelable(true);
        btnAgreeAndContinues = (Button) dialogTerms.findViewById(R.id.btn_agree_and_continue);
        btnAgreeAndContinues.setOnClickListener(this);
    }

    void showTermsDialog(){
        dialogTerms.show();
    }

    void hideTermsDialog(){
        dialogTerms.hide();
    }

    private void navToForgetPasswordFragment() {
        Fragment frg = new ForgetPasswordFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_intro, frg, AppConstt.FRGTAG.ForgetPasswordFragment);
        ft.addToBackStack(AppConstt.FRGTAG.ForgetPasswordFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.txv_reset_passw:
                navToForgetPasswordFragment();
                break;
            case R.id.txv_term_of_policy:
                showTermsDialog();
                break;
            case R.id.btn_agree_and_continue:
                hideTermsDialog();
                break;
        }
    }
}
