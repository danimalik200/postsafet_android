package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 27/06/2018.
 */

public class NoConnectionFragment  extends Fragment implements View.OnClickListener{
    Button btnBackToReport;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_no_conenction, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        btnBackToReport = (Button) v.findViewById(R.id.frg_no_connection_btn_return_to);
        btnBackToReport.setOnClickListener(this);

    }

    private void navToDashboardFragment() {
        Fragment frg = new DashboardFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        for (int i = 0; i < count; i++) {
            fm.popBackStackImmediate();
        }
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_home, frg, AppConstt.FRGTAG.DashboardFragment);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frg_no_connection_btn_return_to:
                navToDashboardFragment();
                break;
        }
    }
}
