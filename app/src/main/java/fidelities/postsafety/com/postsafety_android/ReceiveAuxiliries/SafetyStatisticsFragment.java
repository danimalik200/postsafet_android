package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class SafetyStatisticsFragment extends Fragment implements View.OnClickListener{
    LinearLayout ll_yesterday,ll_this_month, ll_year_to_month;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_safety_statistics, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){

        ll_yesterday = (LinearLayout) v.findViewById(R.id.ll_yesterday);
        ll_this_month = (LinearLayout) v.findViewById(R.id.ll_this_month);
        ll_year_to_month = (LinearLayout) v.findViewById(R.id.ll_year_to_date);

        ll_yesterday.setOnClickListener(this);
        ll_this_month.setOnClickListener(this);
        ll_year_to_month.setOnClickListener(this);


    }

    private void navToSafetyReportTypeFragment() {
        Fragment frg = new SafetyReportTypeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.SafetyReportTypeFragment);
        ft.addToBackStack(AppConstt.FRGTAG.SafetyReportTypeFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_yesterday:
                AppConfig.getInstance().isComingFromSafetyStatistics =true;
                AppConfig.getInstance().safetyStatisticType ="Yesterday";
                navToSafetyReportTypeFragment();
                break;
            case R.id.ll_this_month:
                AppConfig.getInstance().isComingFromSafetyStatistics =true;
                AppConfig.getInstance().safetyStatisticType ="This Month";
                navToSafetyReportTypeFragment();
                break;
            case R.id.ll_year_to_date:
                AppConfig.getInstance().isComingFromSafetyStatistics =true;
                AppConfig.getInstance().safetyStatisticType ="This Year";
                navToSafetyReportTypeFragment();
                break;
        }
    }
}
