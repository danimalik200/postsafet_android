package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class AdimnReceiveHomeFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_alerts, ll_view_posts, ll_training, ll_policies, ll_safety;
    ToolbarUpdateListener toolbarUpdateListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_admin_receive_home, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        ll_alerts = (LinearLayout) v.findViewById(R.id.ll_alerts);
        ll_view_posts = (LinearLayout) v.findViewById(R.id.ll_view_posts);
        ll_training = (LinearLayout) v.findViewById(R.id.ll_training);
        ll_policies = (LinearLayout) v.findViewById(R.id.ll_policies);
        ll_safety = (LinearLayout) v.findViewById(R.id.ll_safety_stats);

        ll_alerts.setOnClickListener(this);
        ll_view_posts.setOnClickListener(this);
        ll_training.setOnClickListener(this);
        ll_policies.setOnClickListener(this);
        ll_safety.setOnClickListener(this);
        toolbarUpdateListener = (ToolbarUpdateListener) getActivity();
        toolbarUpdateListener.backButtonVisibilty(View.GONE);
        toolbarUpdateListener.hideToolbar(View.VISIBLE);

    }

    private void navToAlertsHomeFragment() {
        Fragment frg = new AlertsHomeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.AlertsHomeFragment);
        ft.addToBackStack(AppConstt.FRGTAG.AlertsHomeFragment);
        ft.hide(this);
        ft.commit();
    }
    private void navToViewPostsFragment() {
        Fragment frg = new ViewPostsFeedsFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.ViewPostsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.ViewPostsFragment);
        ft.hide(this);
        ft.commit();
    }

    private void navToTrainingHomeFragment() {
        Fragment frg = new TrainingHomeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.TrainingHomeFragment);
        ft.addToBackStack(AppConstt.FRGTAG.TrainingHomeFragment);
        ft.hide(this);
        ft.commit();
    }

    private void navToFeedsFragment(Bundle bundle) {
        Fragment frg = new FeedsFragment();
        frg.setArguments(bundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.FeedsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.FeedsFragment);
        ft.hide(this);
        ft.commit();
    }

    private void navToSafetyStatisticsFragment() {
        Fragment frg = new SafetyStatisticsFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.SafetyStatisticsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.SafetyStatisticsFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        Bundle bundle = new Bundle();
        String alertType ="";
        switch (v.getId()){
            case R.id.ll_alerts:
                navToAlertsHomeFragment();
                break;
            case R.id.ll_view_posts:
                navToViewPostsFragment();
                break;
            case R.id.ll_training:
                navToTrainingHomeFragment();
                break;
            case R.id.ll_policies:
                alertType ="Policies/Procedures";
                bundle.putString("alertTpe", alertType);
                navToFeedsFragment(bundle);

                break;
            case R.id.ll_safety_stats:
                AppConfig.getInstance().isComingFromSafetyStatistics =true;
                navToSafetyStatisticsFragment();
                break;
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(isHidden()){

        }else {
            AppConfig.getInstance().isComingFromSafetyStatistics =false;
        }
    }
}
