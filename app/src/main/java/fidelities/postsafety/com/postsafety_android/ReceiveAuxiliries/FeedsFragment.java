package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class FeedsFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_news_unopened, ll_archived, rl_tabs_cntnr;
    TextView txvNews, txvArchived, txvHeaderText;
    GridView gridViewFeeds;
    ArrayList<DModelFeeds> listFeeds;
    FeedsAdapter adapter;
    Bundle bundle;
    String alertType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_feeds_container, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        ll_news_unopened =(LinearLayout) v.findViewById(R.id.ll_news_unopened);
        ll_archived =(LinearLayout) v.findViewById(R.id.ll_archived);
        rl_tabs_cntnr =(LinearLayout) v.findViewById(R.id.rl_tabs_cntnr);
        txvNews = (TextView) v.findViewById(R.id.txv_new_unopened) ;
        txvArchived = (TextView) v.findViewById(R.id.txv_archived);
        txvHeaderText = (TextView) v.findViewById(R.id.txv_feeds_header);
        gridViewFeeds = (GridView) v.findViewById(R.id.gridview_feeds) ;

        ll_news_unopened.setOnClickListener(this);
        ll_archived.setOnClickListener(this);
        listFeeds = new ArrayList<>();

        bundle = this.getArguments();
        if(bundle!=null){
            alertType =bundle.getString("alertTpe", "");
        }

        if(AppConfig.getInstance().isComingFromSafetyStatistics){
            txvHeaderText.setText(AppConfig.getInstance().safetyStatisticType+"'s"+" "+alertType);
            rl_tabs_cntnr.setVisibility(View.GONE);
        }else {
            txvHeaderText.setText(alertType);
            rl_tabs_cntnr.setVisibility(View.VISIBLE);
        }

        gridViewFeeds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(AppConfig.getInstance().isComingFromSafetyStatistics){

                }else {
                    String alertID = listFeeds.get(position).getId();
                    String alertHeader = listFeeds.get(position).getHeaderText();

                    Bundle bundle = new Bundle();
                    bundle.putString("alertDetail", alertHeader);
                    navToSingleLAertDetailFragment(bundle);
                }

            }
        });

        setUpListItems();

    }

    private void navToSingleLAertDetailFragment(Bundle bundle) {
        Fragment frg = new SingleAlertDetailFragment();
        frg.setArguments(bundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.SingleAlertDetailFragment);
        ft.addToBackStack(AppConstt.FRGTAG.SingleAlertDetailFragment);
        ft.hide(this);
        ft.commit();
    }

    void setUpListItems(){
        DModelFeeds dModelFeeds = new DModelFeeds();
        dModelFeeds.setId("1");
        dModelFeeds.setHeaderText("Training Exercise: Ascending a high...");
        dModelFeeds.setTime("9:53 Am");
        dModelFeeds.setDate("6 May, 2018");
        listFeeds.add(dModelFeeds);


        DModelFeeds dModelFeeds1 = new DModelFeeds();
        dModelFeeds1.setId("2");
        dModelFeeds1.setHeaderText("Training Exercise: How to Access a...");
        dModelFeeds1.setTime("8:53 Pm");
        dModelFeeds1.setDate("12 April, 2018");
        listFeeds.add(dModelFeeds1);


        DModelFeeds dModelFeeds2 = new DModelFeeds();
        dModelFeeds2.setId("3");
        dModelFeeds2.setHeaderText("Training Exercise: Ascending a high...");
        dModelFeeds2.setTime("6:53 Am");
        dModelFeeds2.setDate("16 Mar, 2018");
        listFeeds.add(dModelFeeds2);


        DModelFeeds dModelFeeds3 = new DModelFeeds();
        dModelFeeds3.setId("4");
        dModelFeeds3.setHeaderText("Training Exercise: How to Access a...");
        dModelFeeds3.setTime("1:53 Pm");
        dModelFeeds3.setDate("21 Feb, 2018");
        listFeeds.add(dModelFeeds3);
        adapter = new FeedsAdapter(listFeeds, getActivity());
        gridViewFeeds.setAdapter(adapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_news_unopened:
                updateNewsButton();
                break;
            case R.id.ll_archived:
                updateArchivedButton();
                break;
        }
    }

    void updateNewsButton(){

        ll_news_unopened.setBackgroundResource(R.color.orange2);
        ll_archived.setBackgroundResource(R.color.gray_background);
        txvNews.setTextColor(getResources().getColor(R.color.white));
        txvArchived.setTextColor(getResources().getColor(R.color.black));
    }

    void updateArchivedButton(){

        ll_news_unopened.setBackgroundResource(R.color.gray_background);
        ll_archived.setBackgroundResource(R.color.orange2);
        txvNews.setTextColor(getResources().getColor(R.color.black));
        txvArchived.setTextColor(getResources().getColor(R.color.white));
    }
}
