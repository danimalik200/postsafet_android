package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 26/06/2018.
 */

public class ReportSummaryFragment extends Fragment implements View.OnClickListener {
    Button btnSendReport;
    EditText edtAdditionalDetail;
    ToolbarUpdateListener toolbarUpdateListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report_summary, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){

        btnSendReport = (Button) v.findViewById(R.id.btn_send_report);
        edtAdditionalDetail = (EditText) v.findViewById(R.id.edt_additional_report_detail);
        toolbarUpdateListener =(ToolbarUpdateListener) getActivity();
        toolbarUpdateListener.backButtonVisibilty(View.VISIBLE);
        toolbarUpdateListener.hideToolbar(View.VISIBLE);
        btnSendReport.setOnClickListener(this);

    }

    private void navToConfirmationFragment() {
        Fragment frg = new ConfirmtaionFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.ConfirmationFragment);
        ft.addToBackStack(AppConstt.FRGTAG.ConfirmationFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send_report:
                navToConfirmationFragment();
                break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(isHidden()){

        }else {
            toolbarUpdateListener.hideToolbar(View.VISIBLE);
            toolbarUpdateListener.backButtonVisibilty(View.VISIBLE);
        }
    }
}
