package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class PliciesProceduresFragment extends Fragment implements View.OnClickListener {
    LinearLayout ll_training, ll_definitions;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_training_home, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){

        ll_training =(LinearLayout) v.findViewById(R.id.ll_training);
        ll_definitions = (LinearLayout) v.findViewById(R.id.ll_definitions);

        ll_training.setOnClickListener(this);
        ll_definitions.setOnClickListener(this);

    }

    private void navToFeedsFragment(Bundle bundle) {
        Fragment frg = new FeedsFragment();
        frg.setArguments(bundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.FeedsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.FeedsFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {

        Bundle bundle = new Bundle();
        String alertHeader ="";
        switch (v.getId()){
            case R.id.ll_training:
                alertHeader ="Training";
                bundle.putString("alertTpe", alertHeader);
                navToFeedsFragment(bundle);
                break;
            case R.id.ll_definitions:
                alertHeader = "Definitions";
                bundle.putString("alertTpe", alertHeader);
                navToFeedsFragment(bundle);
                break;
        }
    }
}
