package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class SafetyReportTypeFragment extends Fragment  implements View.OnClickListener {

    LinearLayout ll_hazard, ll_near_mess, ll_incident, ll_emergency;
    TextView txv_hazard, txv_near_miss, txv_incident, txv_emergency;
    ToolbarUpdateListener toolbarUpdateListener;
    Bundle bundle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_safety_statistics_report_type, container, false);
        bindViews(v);
        return v;

    }

    public void bindViews(View v) {
        ll_hazard = (LinearLayout) v.findViewById(R.id.frg_safety_ll_hazard);
        ll_near_mess = (LinearLayout) v.findViewById(R.id.frg_safety_ll_near_miss);
        ll_incident = (LinearLayout) v.findViewById(R.id.frg_safety_ll_incident);
        ll_emergency = (LinearLayout) v.findViewById(R.id.frg_safety_ll_emergency);
        txv_hazard = (TextView) v.findViewById(R.id.txv_hazard);
        txv_near_miss = (TextView) v.findViewById(R.id.txv_near_miss);
        txv_incident = (TextView) v.findViewById(R.id.txv_incident);
        txv_emergency = (TextView) v.findViewById(R.id.txv_emergency);
        toolbarUpdateListener = (ToolbarUpdateListener) getActivity();
        toolbarUpdateListener.backButtonVisibilty(View.GONE);


        ll_hazard.setOnClickListener(this);
        ll_near_mess.setOnClickListener(this);
        ll_incident.setOnClickListener(this);
        ll_emergency.setOnClickListener(this);
         bundle = new Bundle();

    }

    private void navToFeedsFragment(Bundle bundle) {
        Fragment frg = new FeedsFragment();
        frg.setArguments(bundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.FeedsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.FeedsFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frg_safety_ll_hazard:
                Log.d("clicked", "Clicked Hazard");
                bundle.putString("alertTpe", "Hazard");
                navToFeedsFragment(bundle);
                break;
            case R.id.frg_safety_ll_near_miss:
                bundle.putString("alertTpe", "New Miss");
                navToFeedsFragment(bundle);
                break;
            case R.id.frg_safety_ll_incident:
                bundle.putString("alertTpe", "Incident");
                navToFeedsFragment(bundle);
                break;
            case R.id.frg_safety_ll_emergency:
                bundle.putString("alertTpe", "Emergency");
                navToFeedsFragment(bundle);
                break;
        }
    }
}