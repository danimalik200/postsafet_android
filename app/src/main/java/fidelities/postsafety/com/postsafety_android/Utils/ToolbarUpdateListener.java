package fidelities.postsafety.com.postsafety_android.Utils;


/**
 * Created by Lenovo on 27/06/2018.
 */

public interface ToolbarUpdateListener {

    void backButtonVisibilty(int visibility);
    void hideToolbar(int visibility);

}
