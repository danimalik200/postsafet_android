package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;

/**
 * Created by Lenovo on 26/06/2018.
 */

public class SubCategoriesFragment extends Fragment implements View.OnClickListener{
    LinearLayout ll_subcat1, ll_subcat2, ll_subcat3, ll_subcat4;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subcategory, container, false);
        bindViews(v);
        return v;

    }

    void bindViews(View v){

        ll_subcat1  = (LinearLayout) v.findViewById(R.id.ll_subcat1);
        ll_subcat2  = (LinearLayout) v.findViewById(R.id.ll_subcat2);
        ll_subcat3  = (LinearLayout) v.findViewById(R.id.ll_subcat3);
        ll_subcat4  = (LinearLayout) v.findViewById(R.id.ll_subcat4);

        ll_subcat1.setOnClickListener(this);
        ll_subcat2.setOnClickListener(this);
        ll_subcat3.setOnClickListener(this);
        ll_subcat4.setOnClickListener(this);


    }

    private void navtoLocationsFragment() {
        Fragment frg = new LocationsFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.LocationsFragment);
        ft.addToBackStack(AppConstt.FRGTAG.LocationsFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_subcat1:
                navtoLocationsFragment();
                break;
            case R.id.ll_subcat2:
                navtoLocationsFragment();
                break;
            case R.id.ll_subcat3:
                navtoLocationsFragment();
                break;
            case R.id.ll_subcat4:
                navtoLocationsFragment();
                break;
        }
    }
}
