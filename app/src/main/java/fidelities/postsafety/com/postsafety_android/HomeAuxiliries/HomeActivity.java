package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.opengl.Visibility;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import fidelities.postsafety.com.postsafety_android.IntroAuxiliries.SigninFragment;
import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 25/06/2018.
 */

public class HomeActivity extends AppCompatActivity implements View.OnClickListener , ToolbarUpdateListener{
    LinearLayout ll_menu, ll_back_btn;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindViews();
    }

    void bindViews(){
        ll_menu =(LinearLayout) findViewById(R.id.act_home_tb_ll_menu);
        ll_back_btn = (LinearLayout) findViewById(R.id.act_home_tb_ll_back);
        toolbar = (Toolbar) findViewById(R.id.act_home_toolbar) ;

        ll_menu.setOnClickListener(this);
        ll_back_btn.setOnClickListener(this);
        navToDashboardFragment();
    }

    private void navToDashboardFragment() {
        Fragment frg = new DashboardFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_home, frg, AppConstt.FRGTAG.DashboardFragment);
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.act_home_tb_ll_menu:
                break;
            case R.id.act_home_tb_ll_back:
                getSupportFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void backButtonVisibilty(int visibility) {
        if(visibility ==View.VISIBLE){
            ll_back_btn.setVisibility(View.VISIBLE);
            ll_menu.setVisibility(View.GONE);
        }else {
            ll_back_btn.setVisibility(View.GONE);
            ll_menu.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideToolbar(int visibility) {
        if(visibility ==View.VISIBLE){
            toolbar.setVisibility(View.VISIBLE);
        }else {
            toolbar.setVisibility(View.GONE);

        }
    }
}
