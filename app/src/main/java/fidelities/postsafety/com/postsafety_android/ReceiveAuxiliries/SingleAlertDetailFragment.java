package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import fidelities.postsafety.com.postsafety_android.R;

/**
 * Created by Lenovo on 28/06/2018.
 */

public class SingleAlertDetailFragment extends Fragment implements View.OnClickListener {

    Button btnBack;
    TextView txvHeader, txvAlertDetail;
    Bundle bundle;
    String headerTitle;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signle_alert_detail, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        btnBack = (Button) v.findViewById(R.id.frg_single_alrt_detail_btn_back);
        txvHeader = (TextView) v.findViewById(R.id.frg_single_alert_detail_txv_header);
        txvAlertDetail = (TextView) v.findViewById(R.id.frg_single_alert_detail_txv_detail);

        btnBack.setOnClickListener(this);

        bundle = this.getArguments();
        if(bundle!=null){
            headerTitle =bundle.getString("alertDetail");

        }

        txvHeader.setText(headerTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frg_single_alrt_detail_btn_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
