package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

/**
 * Created by Lenovo on 26/06/2018.
 */

public class ReportTypeFragment extends Fragment implements View.OnClickListener {

    LinearLayout ll_hazard, ll_near_mess, ll_incident, ll_emergency;
    TextView txv_hazard, txv_near_miss, txv_incident, txv_emergency;
    ToolbarUpdateListener toolbarUpdateListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report_type, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        ll_hazard = (LinearLayout) v.findViewById(R.id.ll_hazard);
        ll_near_mess = (LinearLayout) v.findViewById(R.id.ll_near_miss);
        ll_incident = (LinearLayout) v.findViewById(R.id.ll_incident);
        ll_emergency = (LinearLayout) v.findViewById(R.id.ll_emergency);
        txv_hazard = (TextView) v.findViewById(R.id.txv_hazard);
        txv_near_miss = (TextView) v.findViewById(R.id.txv_near_miss);
        txv_incident = (TextView) v.findViewById(R.id.txv_incident);
        txv_emergency = (TextView) v.findViewById(R.id.txv_emergency);
        toolbarUpdateListener = (ToolbarUpdateListener) getActivity();
        toolbarUpdateListener.backButtonVisibilty(View.VISIBLE);


        ll_hazard.setOnClickListener(this);
        ll_near_mess.setOnClickListener(this);
        ll_incident.setOnClickListener(this);
        ll_emergency.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_hazard:
                navToPhotoVideoFragment();
                break;
            case R.id.ll_near_miss:
                AppConfig.getInstance().isComingFromNearMiss =true;
                navToPhotoVideoFragment();
                break;
            case R.id.ll_incident:
                navToPhotoVideoFragment();
                break;
            case R.id.ll_emergency:
                AppConfig.getInstance().isComingFromEmergency =true;
                navToPhotoVideoFragment();
                break;
        }
    }

    void updateHazardBackground(){

        ll_hazard.setBackgroundColor(getResources().getColor(R.color.trasparent_red_50));
        ll_near_mess.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_incident.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_emergency.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));

        txv_hazard.setTextColor(getResources().getColor(R.color.white));
        txv_near_miss.setTextColor(getResources().getColor(R.color.orange2));
        txv_incident.setTextColor(getResources().getColor(R.color.orange2));
        txv_emergency.setTextColor(getResources().getColor(R.color.orange2));
    }

    void updateNearMissBackground(){

        ll_hazard.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_near_mess.setBackgroundColor(getResources().getColor(R.color.trasparent_red_50));
        ll_incident.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_emergency.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));

        txv_hazard.setTextColor(getResources().getColor(R.color.orange2));
        txv_near_miss.setTextColor(getResources().getColor(R.color.white));
        txv_incident.setTextColor(getResources().getColor(R.color.orange2));
        txv_emergency.setTextColor(getResources().getColor(R.color.orange2));
    }

    void updateIncidentBackground(){

        ll_hazard.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_near_mess.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_incident.setBackgroundColor(getResources().getColor(R.color.trasparent_red_50));
        ll_emergency.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));

        txv_hazard.setTextColor(getResources().getColor(R.color.orange2));
        txv_near_miss.setTextColor(getResources().getColor(R.color.orange2));
        txv_incident.setTextColor(getResources().getColor(R.color.white));
        txv_emergency.setTextColor(getResources().getColor(R.color.orange2));
    }

    void updateemergencyBackground(){

        ll_hazard.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_near_mess.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_incident.setBackgroundColor(getResources().getColor(R.color.tranparent_black_50));
        ll_emergency.setBackgroundColor(getResources().getColor(R.color.trasparent_red_50));

        txv_hazard.setTextColor(getResources().getColor(R.color.orange2));
        txv_near_miss.setTextColor(getResources().getColor(R.color.orange2));
        txv_incident.setTextColor(getResources().getColor(R.color.orange2));
        txv_emergency.setTextColor(getResources().getColor(R.color.white));
    }


    private void navToPhotoVideoFragment() {
        Fragment frg = new FragmentPhotoVideo();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.PhotoVideoFragment);
        ft.addToBackStack(AppConstt.FRGTAG.PhotoVideoFragment);
        ft.hide(this);
        ft.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(isHidden()){

        }else {
            toolbarUpdateListener.hideToolbar(View.VISIBLE);
        }
    }
}
