package fidelities.postsafety.com.postsafety_android.HomeAuxiliries;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.SessionType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.Utils.AppConfig;
import fidelities.postsafety.com.postsafety_android.Utils.AppConstt;
import fidelities.postsafety.com.postsafety_android.Utils.ToolbarUpdateListener;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * Created by Lenovo on 26/06/2018.
 */

public class FragmentPhotoVideo extends Fragment implements View.OnClickListener {
    CameraView cameraView;
    ImageView imvCapture, imvCross, imvSend;
    private boolean takingVideo = false;
    File file;
    ToolbarUpdateListener toolbarUpdateListener;
    Uri imageUri, picUri;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_video, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        cameraView = (CameraView) v.findViewById(R.id.camera);
        imvCapture = (ImageView) v.findViewById(R.id.imv_capture);
        toolbarUpdateListener =(ToolbarUpdateListener) getActivity();
        toolbarUpdateListener.hideToolbar(View.GONE);

        imvCapture.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if (checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Log.v("", "Permission is granted");
                    //File write logic here

                    cameraView.setSessionType(SessionType.VIDEO);
                    takingVideo = true;
                    int min = 1;
                    int max = 90;



                    Random r = new Random();
                    int i1 = r.nextInt(max - min + 1) + min;
                    File fileDir = new File(AppConstt.IMAGE_DIR_PATH+ i1);
                    if (!fileDir.exists()) {
                        fileDir.mkdirs();
                    }
                    cameraView.startCapturingVideo(fileDir);

                }else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
                return false;
            }
        });
        imvCross = (ImageView) v.findViewById(R.id.imv_cross);
        imvSend =(ImageView) v.findViewById(R.id.imv_send);
        imvCapture.setOnClickListener(this);
        imvCross.setOnClickListener(this);
        imvSend.setOnClickListener(this);
        cameraView.setBottom(R.color.blue);
        cameraView.setCropOutput(false);
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] picture) {
                // Create a bitmap or a file...
                // CameraUtils will read EXIF orientation for you, in a worker thread.
                //CameraUtils.decodeBitmap(picture, ...);

                Bitmap bitmap = null;

                try {
                    int min = 1;
                    int max = 90;


                    Random r = new Random();
                    int i1 = r.nextInt(max - min + 1) + min;

                    picUri = imageUri;

                    bitmap = BitmapFactory.decodeByteArray(picture, 0, picture.length);
                    String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm", Locale.ENGLISH).format(new Date());
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), picUri);
                    bitmap = imageOreintationValidator(bitmap, imageUri.getPath());
                    File f = new File(getActivity().getCacheDir(), "image" + i1 + timeStamp);
                    f.createNewFile();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 10 /*ignored for PNG*/, bos);
                    FileOutputStream fos = new FileOutputStream(f);
                     fos.write(picture);


                    imvCross.setVisibility(View.VISIBLE);
                        imvCross.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }



            }
        });
        //cameraView.capturePicture();
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onVideoTaken(File video) {
                // The File is the same you passed before.
                // Now it holds a MP4 video.
            }
        });
// Select output file. Make sure you have write permissions.
// Later... stop recording. This will trigger onVideoTaken().
        //cameraView.stopCapturingVideo();

        cameraView.setVideoMaxSize(100000);
        cameraView.setVideoMaxDuration(10000);
        cameraView.startCapturingVideo(file);


        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "Post_Safety");
            imagesFolder.mkdirs();


        File image = new File(imagesFolder, "DM_" + timeStamp + ".png");
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        imageUri = Uri.fromFile(image);



    }
    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {
        Log.i("checkrotation", "imageOreintationValidator");
        ExifInterface ei;
        try {
            Log.i("checkrotation", "try");
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    Log.i("checkrotation", "90");
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    Log.i("checkrotation", "180");
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    Log.i("checkrotation", "270");
                    bitmap = rotateImage(bitmap, 270);
                    break;
                default:
                    Log.i("checkrotation", "default");
                    break;
            }
        } catch (IOException e) {
            Log.i("checkrotation", "catch");
            e.printStackTrace();
        }

        return bitmap;
    }

    private Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                    matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        return bitmap;
    }


    @Override
    public void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cameraView.destroy();
    }

    private void navToCategoriesFragment() {
        Fragment frg = new CategoriesFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.content_home, frg, AppConstt.FRGTAG.CategoriesFragment);
        ft.addToBackStack(AppConstt.FRGTAG.CategoriesFragment);
        ft.hide(this);
        ft.commit();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imv_capture:
                if(takingVideo){
                    cameraView.stopCapturingVideo();
                }else {
                    cameraView.capturePicture();
                }
                break;
            case R.id.imv_cross:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.imv_send:
                navToCategoriesFragment();
                break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(isHidden()){

        }else {
            toolbarUpdateListener.hideToolbar(View.GONE);
        }
    }
}
