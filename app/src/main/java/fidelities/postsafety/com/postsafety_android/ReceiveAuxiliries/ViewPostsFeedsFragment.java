package fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import fidelities.postsafety.com.postsafety_android.R;
import fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries.DModelFeeds;
import fidelities.postsafety.com.postsafety_android.ReceiveAuxiliries.FeedsAdapter;

/**
 * Created by Lenovo on 29/06/2018.
 */

public class ViewPostsFeedsFragment extends Fragment implements View.OnClickListener {
    RelativeLayout rl_sort_posts;
    GridView gridViewsPostFeeds;
    LinearLayout ll_shared_posts, ll_my_posts, ll_all_posts;
    TextView txvSharedPosts, txvMyPosts, txvAllPosts;
    ArrayList<DModelFeeds> listPostsFeeds;
    FeedsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_posts_feeds, container, false);
        bindViews(v);
        return v;
    }

    void bindViews(View v){
        rl_sort_posts = (RelativeLayout) v.findViewById(R.id.frg_view_posts_rl_sort_posts);
        gridViewsPostFeeds = (GridView) v.findViewById(R.id.gridview_posts_feeds);
        ll_shared_posts =(LinearLayout) v.findViewById(R.id.ll_shared_posts);
        ll_my_posts = (LinearLayout) v.findViewById(R.id.ll_my_posts);
        ll_all_posts =(LinearLayout) v.findViewById(R.id.ll_all_posts);
        txvSharedPosts =(TextView) v.findViewById(R.id.txv_shared);
        txvMyPosts =(TextView) v.findViewById(R.id.txv_my_posts);
        txvAllPosts =(TextView) v.findViewById(R.id.txv_all_posts);
        listPostsFeeds = new ArrayList<>();


        rl_sort_posts.setOnClickListener(this);
        ll_shared_posts.setOnClickListener(this);
        ll_my_posts.setOnClickListener(this);
        ll_all_posts.setOnClickListener(this);

        setUpListItems();
    }

    void setUpListItems(){
        DModelFeeds dModelFeeds = new DModelFeeds();
        dModelFeeds.setId("1");
        dModelFeeds.setHeaderText("Training Exercise: Ascending a high...");
        dModelFeeds.setTime("9:53 Am");
        dModelFeeds.setDate("6 May, 2018");
        listPostsFeeds.add(dModelFeeds);


        DModelFeeds dModelFeeds1 = new DModelFeeds();
        dModelFeeds1.setId("2");
        dModelFeeds1.setHeaderText("Training Exercise: How to Access a...");
        dModelFeeds1.setTime("8:53 Pm");
        dModelFeeds1.setDate("12 April, 2018");
        listPostsFeeds.add(dModelFeeds1);


        DModelFeeds dModelFeeds2 = new DModelFeeds();
        dModelFeeds2.setId("3");
        dModelFeeds2.setHeaderText("Training Exercise: Ascending a high...");
        dModelFeeds2.setTime("6:53 Am");
        dModelFeeds2.setDate("16 Mar, 2018");
        listPostsFeeds.add(dModelFeeds2);


        DModelFeeds dModelFeeds3 = new DModelFeeds();
        dModelFeeds3.setId("4");
        dModelFeeds3.setHeaderText("Training Exercise: How to Access a...");
        dModelFeeds3.setTime("1:53 Pm");
        dModelFeeds3.setDate("21 Feb, 2018");
        listPostsFeeds.add(dModelFeeds3);
        adapter = new FeedsAdapter(listPostsFeeds, getActivity());
        gridViewsPostFeeds.setAdapter(adapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frg_view_posts_rl_sort_posts:
                break;
            case R.id.ll_shared_posts:
                updateSharedPostsButton();
                break;
            case R.id.ll_all_posts:
                updateAllPostsButton();
                break;
            case R.id.ll_my_posts:
                updateMyPostsButton();
                break;
        }
    }

    void updateSharedPostsButton(){

        ll_shared_posts.setBackgroundResource(R.color.orange2);
        ll_my_posts.setBackgroundResource(R.color.gray_background);
        ll_all_posts.setBackgroundResource(R.color.gray_background);
        txvSharedPosts.setTextColor(getResources().getColor(R.color.white));
        txvMyPosts.setTextColor(getResources().getColor(R.color.black));
        txvAllPosts.setTextColor(getResources().getColor(R.color.black));
    }

    void updateMyPostsButton(){

        ll_shared_posts.setBackgroundResource(R.color.gray_background);
        ll_my_posts.setBackgroundResource(R.color.orange2);
        ll_all_posts.setBackgroundResource(R.color.gray_background);
        txvSharedPosts.setTextColor(getResources().getColor(R.color.black));
        txvMyPosts.setTextColor(getResources().getColor(R.color.white));
        txvAllPosts.setTextColor(getResources().getColor(R.color.black));
    }

    void updateAllPostsButton(){

        ll_shared_posts.setBackgroundResource(R.color.gray_background);
        ll_my_posts.setBackgroundResource(R.color.gray_background);
        ll_all_posts.setBackgroundResource(R.color.orange2);
        txvSharedPosts.setTextColor(getResources().getColor(R.color.black));
        txvMyPosts.setTextColor(getResources().getColor(R.color.black));
        txvAllPosts.setTextColor(getResources().getColor(R.color.white));
    }
}
